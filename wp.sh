#!/bin/bash

echo "============================================"
echo "Instalacion de php7"
echo "============================================"

echo instalacion de php7
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum update -y
 yum install -y mod_php71w php71w-cli php71w-common php71w-gd php71w-mbstring php71w-mcrypt php71w-mysqlnd php71w-xml httpd git


echo "============================================"
echo "Instalacion de mysql y configuracion de base datos"
echo "============================================"
wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm
sudo rpm -ivh mysql-community-release-el7-5.noarch.rpm
yum update -y
yum install mysql-server -y
service mysql start
echo configuracion de mysql root y password
mysql_secure_installation


echo introduce password de mysql para crear base datos y usuario
mysql -uroot -p < script.sql

service mysql restart

echo finalizacion de base datos y php


echo "============================================"
echo "Instalacion de wordpress"
read -p "Database name: " dbname
read -p "Database username: " dbuser
read -p "Enter a password for user $dbuser: " userpass



cd /tmp/
wget -q http://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz 
rm -f /tmp/latest.tar.gz
mv  wordpress/* /var/www/html/

cd /var/www/html/
sed -e "s/database_name_here/"$dbname"/" -e "s/username_here/"$dbuser"/" -e "s/password_here/"$userpass"/" wp-config-sample.php > wp-config.php
chown apache:apache -R /var/www/html

echo permisos de archivos y directorios

find /var/www/html -type d -exec chmod 775 {} \; 	
find /var/www/html -type f -exec chmod 664 {} \; 

echo reinicio de http
service httpd restart





